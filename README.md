## PupilLocation

模型推理是采用腾讯推出的ncnn框架

此文档主要介绍了如何一步步的在ncnn 上执行一个物体检测，模型文件已包含其中。

*ps: 树莓派4b, 内核版本为 5.4*（64bit）

> 部分工具打包下载地址：
>
> https://wws.lanzous.com/b01toj1ch
> 密码:7hdj

### 1 编译安装 openssl

https://www.openssl.org/source/openssl-1.1.1h.tar.gz

```bash
tar -zxvf openssl-1.1.1h.tar.gz    
cd openssl-1.1.1h
./config
make && sudo make install
```

### 2 编译安装 cmake

https://cmake.org/download/

官网下载 arm 对应cmake [下载地址](https://github-production-release-asset-2e65be.s3.amazonaws.com/537699/2b853000-07c6-11eb-8eda-9cdceb80182a?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20201029%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201029T023324Z&X-Amz-Expires=300&X-Amz-Signature=129d3b5f587ce1901d00cb6ebd54da635263efe12cfc395fce62f7a2ae1e3f97&X-Amz-SignedHeaders=host&actor_id=30036938&key_id=0&repo_id=537699&response-content-disposition=attachment%3B%20filename%3Dcmake-3.18.4.tar.gz&response-content-type=application%2Foctet-stream)

进行编译安装：

```bash
tar -zxvf cmake-3.18.4.tar.gz
cd cmake 
./configure
make && sudo make install
```



### 3 编译安装 protobuf

官网下载 protobuf 压缩包 [下载地址](https://github.com/protocolbuffers/protobuf/releases/tag/v3.13.0)，认准  protobuf-cpp-3.13.0.tar.gz 字样

```bash
tar -zxvf protobuf-cpp-3.13.0.tar.gz
cd protobuf-3.13.0/
./configure
make && sudo make install
```



### 4 编译安装 OpenCV for c++

随着 OpenCV 版本变化，安装方式可能发生改变，可参考官网教程： [opencv installation](https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html?highlight=installation)

```bash
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev

git clone https://gitee.com/yijunlog/opencv.git
cd opencv/
mkdir build && cd build
cmake ../
make -j2 && sudo make install
```

> 此版本为临时 fork 到 gitee 上的版本，若需要最新版，可到```https://github.com/opencv/opencv.git```



> 可能需要用到命令：export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

### 5 包含测试程序的ncnn

``` bash
git clone https://gitee.com/yijunlog/pupillocation.git
cd  pupillocation/build
cmake ../
make -j4 && make install
cd yolofastest
./buildyolo.sh
./yolofastest
```
